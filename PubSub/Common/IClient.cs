﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using static Common.Enums;

namespace Common
{
    [ServiceContract]
    public interface IClient
    {
        [OperationContract]
        void Publish(RiskLevel rl);

        [OperationContract]
        void Validate(RiskLevel rl);

        [OperationContract]
        void LogOut(RiskLevel rl);

        [OperationContract]
        string Notify(RiskLevel rl);
    }
}

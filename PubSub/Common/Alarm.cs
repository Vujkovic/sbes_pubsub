﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    [DataContract]
    public class Alarm
    {
        DateTime timeStamp;
        string message;
        int risk;

        public Alarm(DateTime timeStamp, string message, int risk)
        {
            TimeStamp = timeStamp;
            Message = message;
            Risk = risk;
        }

        [DataMember]
        public DateTime TimeStamp { get => timeStamp; set => timeStamp = value; }
        [DataMember]
        public string Message { get => message; set => message = value; }
        [DataMember]
        public int Risk { get => risk; set => risk = value; }
    }
}

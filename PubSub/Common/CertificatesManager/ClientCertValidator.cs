﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.IdentityModel;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Selectors;
using System.Security.Principal;

namespace Common.CertificatesManager
{
    public class ClientCertValidator : X509CertificateValidator
    {
        /// <summary>
        /// Implementation of a custom certificate validation on the client side.
        /// Client should consider certificate valid if the given certifiate is not self-signed.
        /// If validation fails, throw an exception with an adequate message.
        /// </summary>
        /// <param name="certificate"> certificate to be validate </param>
        public override void Validate(X509Certificate2 certificate)
        {
            if (!certificate.Subject.Equals(certificate.Issuer))
            {
                throw new Exception("Certificate is not self-issued.");
            }

            if (certificate.NotAfter < DateTime.Now)
            {
                throw new Exception("Certificate has expired.");
            }

            if (certificate.SubjectName.Name.Equals(String.Format("CN={0}",Formatter.ParseName(WindowsIdentity.GetCurrent().Name))))
            {
                throw new Exception("Certificate has wrong subject name.");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Enums
    {
        public enum RiskLevel
        {
            LowRisk = 0,
            MediumRisk = 1,
            HighRisk = 2,
            VeryHighRisk = 3
        }
    }
}

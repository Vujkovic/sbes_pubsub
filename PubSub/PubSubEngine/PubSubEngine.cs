﻿using Common;
using Common.Audit;
using Common.CertificatesManager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Common.Enums;

namespace PubSubEngine
{
    class PubSubEngine : IPubSubEngine
    {
        public static int id = 1;
        public static List<Tuple<RiskLevel, string, byte[], Alarm>> alarms = new List<Tuple<RiskLevel, string, byte[], Alarm>>();
        public static List<KeyValuePair<string, Alarm>> alarmAlarms = new List<KeyValuePair<string, Alarm>>();
        public static List<KeyValuePair<RiskLevel, string>> logOffs = new List<KeyValuePair<RiskLevel, string>>();

        public void LogOut(RiskLevel rl, string user)
        {
            logOffs.Add(new KeyValuePair<RiskLevel, string>(rl, "Publisher " + user + " logged off"));
        }

        public string Notify(RiskLevel rl)
        {
            string notification = "";

            foreach (KeyValuePair<RiskLevel, string> lo in logOffs)
            {
                if (rl == lo.Key)
                    notification += lo.Value + "\n";
            }

            logOffs = logOffs.Where(a => a.Key != rl).ToList();

            return notification;
        }

        public void Publish(RiskLevel rl, string user, byte[] alarm, Alarm alarmAlarm)
        {
            alarms.Add(new Tuple<RiskLevel, string, byte[], Alarm>(rl, user, alarm, alarmAlarm));
        }

        public Tuple<string, byte[], Alarm> Validate(RiskLevel rl)
        {
            Tuple<string, byte[], Alarm> returnTuple = null;
            Tuple<RiskLevel, string, byte[], Alarm> temp = alarms.FirstOrDefault(a => a.Item1 == rl);
            if (temp != null)
            {
                returnTuple = new Tuple<string, byte[], Alarm>(temp.Item2, temp.Item3, temp.Item4);
            }
            alarms.Remove(temp);

            return returnTuple;
        }

        public void WriteToDB(Alarm alarm, byte[] sign, byte[] key)
        {

            #region WriteToDB
            using (var tw = new StreamWriter("baza.txt", true))
            {
                tw.WriteLine(id + ", " + alarm.TimeStamp + ", " + alarm.Message + ", " + alarm.Risk);
                Audit.Write(id, sign, key);
                File.WriteAllText("id.txt", id.ToString());
                id++;
            }
            #endregion
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using Common;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.ServiceModel.Description;
using System.ServiceModel.Security;
using Common.CertificatesManager;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using Common.Audit;

namespace PubSubEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            //Audit.Hello();
            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;

            string address = "net.tcp://localhost:12321/PubSub";

            ServiceHost host = new ServiceHost(typeof(PubSubEngine));
            host.AddServiceEndpoint(typeof(IPubSubEngine), binding, address);

            string srvCertCN = Formatter.ParseName(WindowsIdentity.GetCurrent().Name);
            //string srvCertCN = "PubSubEngine";

            ///Custom validation mode enables creation of a custom validator - CustomCertificateValidator
            host.Credentials.ClientCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.Custom;
            host.Credentials.ClientCertificate.Authentication.CustomCertificateValidator = new ServiceCertValidator();

            ///If CA doesn't have a CRL associated, WCF blocks every client because it cannot be validated
            host.Credentials.ClientCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;

            ///Set appropriate service's certificate on the host. Use CertManager class to obtain the certificate based on the "srvCertCN"
            host.Credentials.ServiceCertificate.Certificate = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, srvCertCN);

            ServiceSecurityAuditBehavior newAuditBehavior = new ServiceSecurityAuditBehavior();
            newAuditBehavior.ServiceAuthorizationAuditLevel = AuditLevel.SuccessOrFailure;
            newAuditBehavior.MessageAuthenticationAuditLevel = AuditLevel.Success;
            newAuditBehavior.AuditLogLocation = AuditLogLocation.Application;
            host.Description.Behaviors.Remove<ServiceSecurityAuditBehavior>();
            host.Description.Behaviors.Add(newAuditBehavior);

            #region initTxtDb
            string path = "baza.txt";

            if (!File.Exists(path))
            {
                File.Create(path).Close();
            }
            if (File.Exists("id.txt"))
            {
                using (StreamReader sr = new StreamReader("id.txt"))
                {
                    PubSubEngine.id = Int32.Parse(sr.ReadLine());
                    PubSubEngine.id++;
                }
            }
            #endregion

            try
            {
                host.Open();
                Console.WriteLine("PubSubEngine has started. Press <enter> to finish...");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] {0}", e.Message);
                Console.WriteLine("[StackTrace] {0}", e.StackTrace);
            }
            finally
            {
                host.Close();
            }
        }
    }
}

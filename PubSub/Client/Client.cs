﻿using Common;
using Common.CertificatesManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;
using static Common.Enums;

namespace Client
{
    public class Client : ChannelFactory<IPubSubEngine>, IClient, IDisposable
    {
        IPubSubEngine factory;
        public static string[] messages = new string[] { AlarmStandardMessage.Message1, AlarmStandardMessage.Message2, AlarmStandardMessage.Message3, AlarmStandardMessage.Message4, AlarmStandardMessage.Message5 };

        public Client(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            //Debugger.Launch();
            string cltCertCN = Formatter.ParseName(WindowsIdentity.GetCurrent().Name);

            this.Credentials.ServiceCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.Custom;
            this.Credentials.ServiceCertificate.Authentication.CustomCertificateValidator = new ClientCertValidator();
            this.Credentials.ServiceCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;

            /// Set appropriate client's certificate on the channel. Use CertManager class to obtain the certificate based on the "cltCertCN"
            this.Credentials.ClientCertificate.Certificate = CertManager.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, cltCertCN + "_sign");

            factory = this.CreateChannel();
        }

        #region Publish()

        public void Publish(RiskLevel rl)
        {
            try
            {
                #region GenerateAlarm
                int risk = 0;
                Random rnd = new Random();
                if (rl == RiskLevel.LowRisk)
                {
                    risk = rnd.Next(1, 11);
                }
                else if (rl == RiskLevel.MediumRisk)
                {
                    risk = rnd.Next(11, 31);
                }
                else if (rl == RiskLevel.HighRisk)
                {
                    risk = rnd.Next(31, 61);

                }
                else if (rl == RiskLevel.VeryHighRisk)
                {
                    risk = rnd.Next(61, 101);
                }

                Alarm alarmAlarm = new Alarm(DateTime.Now, messages[rnd.Next(0, 5)], risk);
                byte[] alarm = DigitalSignature.Create(alarmAlarm.TimeStamp + ", " + alarmAlarm.Message + ", " + alarmAlarm.Risk, "SHA1", this.Credentials.ClientCertificate.Certificate);
                #endregion

                factory.Publish(rl, Formatter.ParseName(WindowsIdentity.GetCurrent().Name), alarm, alarmAlarm);
                Console.WriteLine("Publish allowed.");
            }
            catch (SecurityAccessDeniedException e)
            {
                Console.WriteLine("Error while trying to Read. Error message : {0}", e.Message);
            }
        }

        #endregion

        #region Validate()
        public void Validate(RiskLevel rl)
        {
            try
            {
                Tuple<string, byte[], Alarm> data = factory.Validate(rl);
                if (data != null)
                {
                    X509Certificate2 certificate = CertManager.GetCertificateFromStorage(StoreName.TrustedPeople, StoreLocation.LocalMachine, data.Item1 + "_sign");
                    if (DigitalSignature.Verify(data.Item3.TimeStamp + ", " + data.Item3.Message + ", " + data.Item3.Risk, "SHA1", data.Item2, certificate))
                    {
                        factory.WriteToDB(data.Item3, data.Item2, certificate.PublicKey.EncodedKeyValue.RawData);
                        Console.WriteLine("WriteToDB allowed.");
                    }
                    Console.WriteLine("Validate allowed.");
                }
            }
            catch (SecurityAccessDeniedException e)
            {
                Console.WriteLine("Error while trying to Read. Error message : {0}", e.Message);
            }
        }
        #endregion

        #region LogOut()
        public void LogOut(RiskLevel rl)
        {
            try
            {
                factory.LogOut(rl, Formatter.ParseName(WindowsIdentity.GetCurrent().Name));
                Console.WriteLine("LogOut allowed.");
            }
            catch (SecurityAccessDeniedException e)
            {
                Console.WriteLine("Error while trying to Read. Error message : {0}", e.Message);
            }
        }
        #endregion

        #region Notify()
        public string Notify(RiskLevel rl)
        {
            string notification = "";
            try
            {
                notification = factory.Notify(rl);
                Console.WriteLine("Notify allowed.");
            }
            catch (SecurityAccessDeniedException e)
            {
                Console.WriteLine("Error while trying to Read. Error message : {0}", e.Message);
            }
            return notification;
        }
        #endregion

        public void Dispose()
        {
            if (factory != null)
            {
                factory = null;
            }

            this.Close();
        }
    }
}

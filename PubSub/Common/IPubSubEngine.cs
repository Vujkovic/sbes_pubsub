﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using static Common.Enums;

namespace Common
{
    [ServiceContract]
    public interface IPubSubEngine
    {
        [OperationContract]
        void Publish(RiskLevel rl, string user, byte[] alarm, Alarm alarmAlarm);

        [OperationContract]
        Tuple<string, byte[], Alarm> Validate(RiskLevel rl);

        [OperationContract]
        void WriteToDB(Alarm alarm, byte[] sign, byte[] key);

        [OperationContract]
        void LogOut(RiskLevel rl, string user);

        [OperationContract]
        string Notify(RiskLevel rl);
    }
}

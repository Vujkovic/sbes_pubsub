﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Common.Audit
{
    public class Audit : IDisposable
    {
        private static EventLog customLog = null;
        const string SourceName = "PubSub.Audit";
        const string LogName = "PubSubLog";

        static Audit()
        {
            try
            {
                if (!EventLog.SourceExists(SourceName))
                {
                    EventLog.CreateEventSource(SourceName, LogName);
                }
                customLog = new EventLog(LogName, Environment.MachineName, SourceName);
            }
            catch (Exception e)
            {
                customLog = null;
                Console.WriteLine("Error while trying to create log handle. Error = {0}", e.Message);
            }
        }


        public static void Write(int id, byte[] sign, byte[] key)
        {
            //string signText = Encoding.UTF8.GetString(sign);
            //string keyText = Encoding.UTF8.GetString(key);
            string signText = BitConverter.ToString(sign);
            string keyText = BitConverter.ToString(key);
            if (customLog != null)
            {
                // string message -> create message based on UserAuthenticationSuccess and params
                // write message in customLog, EventLogEntryType is Information or SuccessAudit 
                customLog.WriteEntry("TimeStamp: " + DateTime.Now 
                    + "\n DBName: baza.txt \n" 
                    + "DBId: " +id + "\n" 
                    + "Sign: " + signText + "\n" 
                    + "PublicKey: " + keyText + "\n");
            } 
            else
            {
                throw new ArgumentException("Error while trying to write event to event log.");
            }
        }

        public static void Hello()
        {
            if (customLog != null)
            {
                // string message -> create message based on UserAuthenticationSuccess and params
                // write message in customLog, EventLogEntryType is Information or SuccessAudit 
                customLog.WriteEntry("Hello");
            }
            else
            {
                throw new ArgumentException("Error while trying to write event to event log.");
            }
        }


        public void Dispose()
        {
            if (customLog != null)
            {
                customLog.Dispose();
                customLog = null;
            }
        }
    }
}

﻿using Common;
using Common.CertificatesManager;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static Common.Enums;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            //Debugger.Launch();

            string srvCertCN = "PubSubEngine";

            /// Define the expected certificate for signing ("<username>_sign" is the expected subject name).
            /// .NET WindowsIdentity class provides information about Windows user running the given process
            string signCertCN = String.Empty;

            /// Define subjectName for certificate used for signing which is not as expected by the service
            string wrongCertCN = String.Empty;



            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;

            /// Use CertManager class to obtain the certificate based on the "srvCertCN" representing the expected service identity.
            X509Certificate2 srvCert = CertManager.GetCertificateFromStorage(StoreName.TrustedPeople, StoreLocation.LocalMachine, srvCertCN);
            EndpointAddress address = new EndpointAddress(new Uri("net.tcp://localhost:12321/PubSub"),
                                      new X509CertificateEndpointIdentity(srvCert));


            string clientRole = Formatter.ParseName(WindowsIdentity.GetCurrent().Name);

            string ps = "";
            if (clientRole == "Publisher")
            {
                ps = "publish";
            }
            if (clientRole == "Subscriber")
            {
                ps = "subscribe";
            }

            
            using (Client proxy = new Client(binding, address))
            {
                bool runOutter = true;
                while (runOutter)
                {
                    #region Menu
                    Console.WriteLine("Chose risk level you {0}:", ps);
                    Console.WriteLine("1. 1-10");
                    Console.WriteLine("2. 11-30");
                    Console.WriteLine("3. 31-60");
                    Console.WriteLine("4. 61-100");
                    Console.WriteLine("Press q to exit");
                    string risk = Console.ReadLine();
                    #endregion

                    #region SetRiskLevel
                    RiskLevel rl = RiskLevel.LowRisk;
                    if (risk == "1")
                    {
                        rl = RiskLevel.LowRisk;
                    }
                    else if (risk == "2")
                    {
                        rl = RiskLevel.MediumRisk;
                    }
                    else if (risk == "3")
                    {
                        rl = RiskLevel.HighRisk;
                    }
                    else if (risk == "4")
                    {
                        rl = RiskLevel.VeryHighRisk;
                    }
                    else if( risk == "q")
                    {
                        runOutter = false;
                        break;
                    }
                    #endregion

                    #region Interval
                    int interval = 0;
                    while(interval == 0 && clientRole == "Publisher")
                    {
                        Console.WriteLine("Chose interval in milliseconds in which you wish to send data");
                        Int32.TryParse(Console.ReadLine(), out interval);
                    } 
                    #endregion

                    if (clientRole == "Publisher")
                    {
                        bool run = true;
                        Console.WriteLine("Press q to quit");
                        Thread t = new Thread(() =>
                        {
                            string q = Console.ReadLine();
                            if (q == "q")
                            {
                                Task.Factory.StartNew(() => proxy.LogOut(rl)).Wait();
                                run = false;
                            }
                        });
                        t.Start();
                        while (run)
                        {
                            Task.Factory.StartNew(() => proxy.Publish(rl)).Wait();
                            Thread.Sleep(interval);
                        }
                        t.Abort();
                    }
                    if (clientRole == "Subscriber")
                    {
                        bool run = true;
                        Console.WriteLine("Press q to quit");
                        Thread t = new Thread(() =>
                        {
                            string q = Console.ReadLine();
                            if (q == "q")
                            {
                                run = false;
                            }
                        });
                        t.Start();
                        string notification = "";
                        while (run)
                        {
                            Task.Factory.StartNew(() => proxy.Validate(rl)).Wait();
                            Task.Factory.StartNew(() => notification = proxy.Notify(rl)).Wait();
                            if (!String.IsNullOrEmpty(notification))
                            {
                                Console.WriteLine(notification);
                            }
                            Thread.Sleep(1000);
                        }
                        t.Abort();
                    }
                }
            }
            Console.ReadLine();
        }
    }
}
